var app = angular.module('AirWander', []);

app.controller('AirWanderController', HomeController);

function HomeController ($scope) {

	/*
		values will be shown in dropdown options
		inputs are list of objects containing inputs behavior
			- oneWay is one way binding applies input oneway-value
			- twoWay is two way binding applies input ng-model
	*/
	$scope.trips = [
		{
			value: 'Type of Trip',
			inputs: [{}, {}]
		},
		{
			value: 'One-Way',
			inputs: [{
				id: 'input1'
			}, {
				id: 'input2'
			}]
		},
		{
			value: 'Round-Trip',
			inputs: [{
				id: 'input1',
				value: '',
				twoWay: 'duplicate'
			}, 
			{
				id: 'input2'
			},
			{	id: 'input3',
				value: '',
				oneWay: 'duplicate'
			}]
		},
		{
			value: 'Multi-City',
			inputs: [{
				id: 'input1'
			}, {
				id: 'input2'
			}, {
				id: 'input3'
			}]
		},
		{
			value: 'World-Tour',
			inputs: [{
				id: 'input1'
			}, {
				id: 'input2'
			}, {
				id: 'input3'
			}, {
				id: 'input4'
			}]
		}
	];

	$scope.selectedItem = $scope.trips[0];
}

// small directive to allow for one way binding in input
app.directive('onewayValue', function() {
  return function(scope, elem, attrs) {
    scope.$watch(attrs.onewayValue, elem.val.bind(elem));
  };
})
