app.directive('inputBoxElement', function () {

    return {
        restrict: 'E',
        scope: {
        	item: "="
        },
        templateUrl: './input-box-element/app.input-box-element.html',
        require: "^inputBox",
        link: function (scope, element, attr, ParentCtr) {
            scope.updateBox = function (value) {
                ParentCtr.updateModel(value, scope.item.twoWay);
            }
        }
    }
});