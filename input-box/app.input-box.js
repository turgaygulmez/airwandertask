app.directive('inputBox', function () {

    return {
        restrict: 'E',
        scope: { 
        	elements: "="
        },
        templateUrl: './input-box/app.input-box.html',
        controller: function($scope, $element, $attrs, $transclude) { 
        	this.updateModel= function (newValue, bindingName) {
                // loop through elements model and update twoway and oneway binding inputs
                for (var i = $scope.elements.length - 1; i >= 0; i--) {
                    if ($scope.elements[i].twoWay === bindingName) {
                        $scope.elements[i].value = newValue;
                    }

                    if ($scope.elements[i].oneWay === bindingName) {
                        $scope.elements[i].value = newValue;
                    }
                }
            }
        }
    }
});